package com.beam.pipelines;

import java.nio.file.Paths;
import java.util.List;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.Sum;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;
import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

import com.beam.transformers.KeyValueToStringConverter;

public class AccountDeptWiseSummarySideInputPipeline {

	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\dept_data_tmp.txt";

	private static final String FILE_EXCLUDE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\exclude_ids.txt";

	
	private static final String OUTPUT_FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-side-input-account.txt";

	public static class MyTransformation extends PTransform<PCollection<String>, PCollection<String>> {
		private static final long serialVersionUID = 1L;
		private PCollectionView<List<String>> toExclude;

		public MyTransformation(PCollectionView<List<String>> toExclude) {
			super();
			this.toExclude = toExclude;
		}
		@Override
		public PCollection<String> expand(PCollection<String> input) {
			return input.apply(ParDo.of(new DoFn<String, String[]>() {
				private static final long serialVersionUID = 1L;

				@ProcessElement
				public void processElement(@Element String element, OutputReceiver<String[]> receiver,
						ProcessContext c) {
					// Output each word encountered into the output PCollection.
					List<String> toExcludeList = c.sideInput(toExclude);
					String[] input = element.split(",");
					if (input[3].equals("Accounts") && !toExcludeList.contains(input[0])) {
						receiver.output(input);
					}
				}
			}).withSideInput("to_exclude", toExclude)).apply(new MyCompositTransform());
		}

	}
	
	/**
	 * 
	 */
	private static class MyCompositTransform extends PTransform<PCollection<String[]>, PCollection<String>> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public @UnknownKeyFor @NonNull @Initialized PCollection<String> expand(
				@UnknownKeyFor @NonNull @Initialized PCollection<String[]> input) {

			return input.apply("Convert_to_Name_Count_Account",
					MapElements.via(new SimpleFunction<String[], KV<String, Long>>() {
						private static final long serialVersionUID = 1L;

						@Override
						public @UnknownKeyFor @NonNull @Initialized KV<String, Long> apply(
								String @UnknownKeyFor @NonNull @Initialized [] input) {
							return KV.of(input[1], 1l);
						}

					})).apply("Group_By_Key_For_Account", Sum.longsPerKey())
					.apply("Convert_Key_To_String_Account", MapElements.via(new KeyValueToStringConverter<>()));

		}

	}
	
	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline accountPipeline = Pipeline.create(options);
		PCollectionView<List<String>> toExcludeView = accountPipeline.apply(TextIO.read().from(FILE_EXCLUDE))
				.apply(View.asList());
		accountPipeline.apply(TextIO.read().from(FILE)).apply(new MyTransformation(toExcludeView))
				.apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE).toString()));

		accountPipeline.run().waitUntilFinish();
	}
}
