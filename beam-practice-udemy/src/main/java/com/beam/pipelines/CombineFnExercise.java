package com.beam.pipelines;

import java.io.Serializable;
import java.nio.file.Paths;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Combine;
import org.apache.beam.sdk.transforms.Combine.CombineFn;
import org.apache.beam.sdk.transforms.Create;

import com.google.common.collect.Lists;

public class CombineFnExercise {
	private static final String OUTPUT_FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\average.txt";

	public static class AverageFn extends CombineFn<Integer, AverageFn.Accum, String> implements Serializable {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		public static class Accum implements Serializable {
			int sum = 0;
			int count = 0;

			/**
			 * @return the sum
			 */
			public int getSum() {
				return sum;
			}

			/**
			 * @param sum the sum to set
			 */
			public void setSum(int sum) {
				this.sum = sum;
			}

			/**
			 * @return the count
			 */
			public int getCount() {
				return count;
			}

			/**
			 * @param count the count to set
			 */
			public void setCount(int count) {
				this.count = count;
			}

		}

		@Override
		public Accum createAccumulator() {
			return new Accum();
		}

		@Override
		public Accum addInput(Accum mutableAccumulator, Integer input) {
			mutableAccumulator.setSum(mutableAccumulator.getSum() + input);
			mutableAccumulator.setCount(mutableAccumulator.getCount() + 1);
			return mutableAccumulator;
		}

		@Override
		public Accum mergeAccumulators(Iterable<Accum> accumulators) {
			Accum mergedAccum = new Accum();
			accumulators.forEach(acc -> {
				mergedAccum.setSum(mergedAccum.getSum() + acc.getSum());
				mergedAccum.setCount(mergedAccum.getCount() + acc.getCount());
			});
			return mergedAccum;
		}

		@Override
		public String extractOutput(Accum accumulator) {
			return ((double) accumulator.getSum() / accumulator.getCount()) + "";
		}

	}

	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline p = Pipeline.create(options);
		p.apply(Create.of(Lists.newArrayList(15, 5, 7, 7, 9, 23, 13, 5))).apply(Combine.globally(new AverageFn()))
				.apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE).toString()));
		
		p.run().waitUntilFinish();
	}
}
