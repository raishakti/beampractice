package com.beam.pipelines;

import java.nio.file.Paths;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.Sum;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

import com.beam.transformers.KeyValueToStringConverter;

/**
 * Example of Branching Acyclic graph of two branches one branch will transform
 * splitted lines for Accounts Other branch will perform splitted lines HR
 */
public class AccountHRDepartmentWiseSummaryPipeline {

	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\dept_data.txt";

	private static final String OUTPUT_FILE_ACCOUNT = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-branch-account.txt";

	private static final String OUTPUT_FILE_HR = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-branch-hr.txt";

	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline p = Pipeline.create(options);

		PCollection<String[]> splittedLinesCollection = p.apply("ReadFromFile", TextIO.read().from(FILE))
				.apply(MapElements.via(new SimpleFunction<String, String[]>() {
					private static final long serialVersionUID = 1L;

					@Override
					public String @UnknownKeyFor @NonNull @Initialized [] apply(
							@UnknownKeyFor @NonNull @Initialized String input) {
						return input.split(",");
					}

				}));

		splittedLinesCollection.apply("FilterForAccount", Filter.by(lineArr -> lineArr[3].equals("Accounts")))
				.apply("Convert_to_Name_Count_Account",
						MapElements.via(new SimpleFunction<String[], KV<String, Long>>() {
							private static final long serialVersionUID = 1L;

							@Override
							public @UnknownKeyFor @NonNull @Initialized KV<String, Long> apply(
									String @UnknownKeyFor @NonNull @Initialized [] input) {
								return KV.of(input[1], 1l);
							}

						}))
				.apply("Group_By_Key_For_Account", Sum.longsPerKey())
				.apply("Convert_Key_To_String_Account", MapElements.via(new KeyValueToStringConverter<>()))
				.apply("Write_Account", TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE_ACCOUNT).toString()));

		splittedLinesCollection.apply("FilterForHR", Filter.by(lineArr -> lineArr[3].equals("HR")))
				.apply("Convert_to_Name_Count_HR", MapElements.via(new SimpleFunction<String[], KV<String, Long>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public @UnknownKeyFor @NonNull @Initialized KV<String, Long> apply(
							String @UnknownKeyFor @NonNull @Initialized [] input) {
						return KV.of(input[1], 1l);
					}

				})).apply("Group_By_Key_For_HR", Sum.longsPerKey())
				.apply("Convert_Key_To_String_HR", MapElements.via(new KeyValueToStringConverter<>()))
				.apply("Write_HR", TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE_HR).toString()));

		p.run().waitUntilFinish();
	}
}
