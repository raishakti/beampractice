package com.beam.pipelines;

import java.nio.file.Paths;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.Sum;
import org.apache.beam.sdk.values.KV;
import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

public class AccountDepartmentWiseSummaryPipeline {

	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\dept_data.txt";

	private static final String OUTPUT_FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-account.txt";

	
	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline accountPipeline = Pipeline.create(options);

		accountPipeline.apply(TextIO.read().from(FILE)).apply(MapElements.via(new SimpleFunction<String, String[]>() {
			private static final long serialVersionUID = 1L;

			@Override
			public String @UnknownKeyFor @NonNull @Initialized [] apply(
					@UnknownKeyFor @NonNull @Initialized String input) {
				return input.split(",");
			}

		})).apply(Filter.by(lineArr -> lineArr[3].equals("Accounts")))
				.apply(MapElements.via(new SimpleFunction<String[], KV<String, Long>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public @UnknownKeyFor @NonNull @Initialized KV<String, Long> apply(
							String @UnknownKeyFor @NonNull @Initialized [] input) {
						return KV.of(input[1], 1l);
					}

				})).apply(Sum.longsPerKey()).apply(MapElements.via(new SimpleFunction<KV<String, Long>, String>() {

					private static final long serialVersionUID = 1L;

					@Override
					public @UnknownKeyFor @NonNull @Initialized String apply(
							@UnknownKeyFor @NonNull @Initialized KV<String, Long> input) {
						return input.getKey() + ": " + input.getValue();
					}

				})).apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE).toString()));
		
		accountPipeline.run().waitUntilFinish();
	}
}
