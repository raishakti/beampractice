package com.beam.pipelines;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.Sum;
import org.apache.beam.sdk.values.KV;
import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

import com.beam.transformers.KeyValueToStringConverter;

public class CreditCardSkipper {

	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\cards.txt";

	private static final String OUTPUT_FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\card_skipper.txt";

	public static void main(String[] args) {

		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline p = Pipeline.create(options);

		p.apply(TextIO.read().from(FILE).withSkipHeaderLines(1))
				.apply(MapElements.via(new SimpleFunction<String, KV<String, Integer>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public @UnknownKeyFor @NonNull @Initialized KV<String, Integer> apply(
							@UnknownKeyFor @NonNull @Initialized String input) {

						List<String> rowData = Arrays.asList(input.split(","));
						double totalSpent = Double.parseDouble(rowData.get(6));
						double totalPaid = Double.parseDouble(rowData.get(8));
						double maxLimit = Double.parseDouble(rowData.get(5));

						double totalPayRequired = totalSpent * .7;
						int point = 0;
						if (totalPaid < totalPayRequired) {
							point = 1;
						}
						if ((totalPaid < totalSpent) && (maxLimit == totalSpent)) {
							point = 1;
						}

						if ((maxLimit == totalSpent) && (totalPaid < totalPayRequired)) {
							point++;
						}

						return KV.of(rowData.get(0) + "-" + rowData.get(1) + "-" + rowData.get(2), point);
					}

				})).apply(Filter.by( v -> v.getValue() > 0)).apply(Sum.integersPerKey()).apply(MapElements.via(new KeyValueToStringConverter<>()))
				.apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE).toString()));
		
		p.run().waitUntilFinish();

	}
}
