package com.beam.pipelines;

import java.nio.file.Paths;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Filter;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.PTransform;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.Sum;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

import com.beam.transformers.KeyValueToStringConverter;

public class CompositeTransform {
	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\dept_data.txt";

	private static final String OUTPUT_FILE_AC_TRANSFORM = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-composit_Account.txt";
	private static final String OUTPUT_FILE_HR_TRANSFORM = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-composit_HR.txt";

	/**
	 * 
	 */
	private static class MyCompositTransform extends PTransform<PCollection<String[]>, PCollection<String>> {

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public @UnknownKeyFor @NonNull @Initialized PCollection<String> expand(
				@UnknownKeyFor @NonNull @Initialized PCollection<String[]> input) {

			return input.apply("Convert_to_Name_Count",
					MapElements.via(new SimpleFunction<String[], KV<String, Long>>() {
						private static final long serialVersionUID = 1L;

						@Override
						public @UnknownKeyFor @NonNull @Initialized KV<String, Long> apply(
								String @UnknownKeyFor @NonNull @Initialized [] input) {
							return KV.of(input[1], 1l);
						}

					})).apply("Group_By_Key", Sum.longsPerKey())
					.apply("Convert_Key_To_String", MapElements.via(new KeyValueToStringConverter<>()));

		}

	}

	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline p = Pipeline.create(options);

		PCollection<String[]> splittedLinesCollection = p.apply("ReadFromFile", TextIO.read().from(FILE))
				.apply(MapElements.via(new SimpleFunction<String, String[]>() {
					private static final long serialVersionUID = 1L;

					@Override
					public String @UnknownKeyFor @NonNull @Initialized [] apply(
							@UnknownKeyFor @NonNull @Initialized String input) {
						return input.split(",");
					}

				}));

		splittedLinesCollection.apply("FilterForAccount", Filter.by(lineArr -> lineArr[3].equals("Accounts")))
				.apply(new MyCompositTransform()).apply("Write_Account",
						TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE_AC_TRANSFORM).toString()));

		splittedLinesCollection.apply("FilterForHR", Filter.by(lineArr -> lineArr[3].equals("HR")))
				.apply(new MyCompositTransform())
				.apply("Write_HR", TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE_HR_TRANSFORM).toString()));

		p.run().waitUntilFinish();
	}
}
