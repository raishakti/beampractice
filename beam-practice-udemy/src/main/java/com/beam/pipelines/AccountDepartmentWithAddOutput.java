package com.beam.pipelines;

import java.nio.file.Paths;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Distinct;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollectionTuple;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.beam.sdk.values.TupleTagList;

public class AccountDepartmentWithAddOutput {

	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\dept_data.txt";

	private static final String OUTPUT_FILE_NAME_LESS4 = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-name-less4.txt";
	private static final String OUTPUT_FILE_NAME_ALL = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\out-name.txt";

	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline accountPipeline = Pipeline.create(options);

		TupleTag<String> nameLessThan = new TupleTag<>() {
		};
		TupleTag<String> nameTag = new TupleTag<>() {
		};

		PCollectionTuple res = accountPipeline.apply(TextIO.read().from(FILE))
				.apply(ParDo.of(new DoFn<String, String>() {
					private static final long serialVersionUID = 1L;

					@ProcessElement
					public void processElement(@Element String element, MultiOutputReceiver out) {
						String[] eleArr = element.split(",");
						if (eleArr[1].length() <= 4) {
							out.get(nameLessThan).output(eleArr[1]);
						}
						out.get(nameTag).output(eleArr[1]);
					}
				}).withOutputTags(nameTag, TupleTagList.of(nameLessThan)));

		res.get(nameLessThan).apply(Distinct.create())
				.apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE_NAME_LESS4).toString()));
		res.get(nameTag).apply(Distinct.create())
				.apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE_NAME_ALL).toString()));
		// .apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE).toString()));

		accountPipeline.run().waitUntilFinish();
	}
}
