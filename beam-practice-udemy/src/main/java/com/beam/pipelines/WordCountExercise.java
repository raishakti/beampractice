package com.beam.pipelines;

import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Count;
import org.apache.beam.sdk.transforms.FlatMapElements;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.values.PCollection;
import org.checkerframework.checker.initialization.qual.Initialized;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.checkerframework.checker.nullness.qual.UnknownKeyFor;

import com.beam.transformers.KeyValueToStringConverter;

public class WordCountExercise {

	public static final String TOKENIZER_PATTERN = "[^\\p{L}]+";
	private static final String FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\data.txt";
	private static final String OUTPUT_FILE = "C:\\Temp\\Gameduell\\workspace\\BeamPractice\\beam-practice-udemy\\src\\main\\resources\\word_count.txt";

	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.fromArgs(args).create();
		Pipeline wordCountPipeline = Pipeline.create(options);

		PCollection<String> words = wordCountPipeline.apply(TextIO.read().from(FILE))
				.apply(FlatMapElements.via(new SimpleFunction<String, List<String>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public List<String> apply(@UnknownKeyFor @NonNull @Initialized String input) {
						return Arrays.asList(input.split(TOKENIZER_PATTERN));
					}

				}));

		words.apply(Count.perElement()).apply(MapElements.via(new KeyValueToStringConverter<>()))
				.apply(TextIO.write().withoutSharding().to(Paths.get(OUTPUT_FILE).toString()));
		wordCountPipeline.run().waitUntilFinish();
	}
}
