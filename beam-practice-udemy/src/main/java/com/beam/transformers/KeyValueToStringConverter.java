package com.beam.transformers;

import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.values.KV;

public class KeyValueToStringConverter<K, V> extends SimpleFunction<KV<K, V>, String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public KeyValueToStringConverter() {
		super((kv) -> kv.getKey().toString() + ": " + kv.getValue());
	}
}
