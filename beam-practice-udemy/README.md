# ApacheBeam Practice Exercises


### beam-practice-udemy
This project contains small exercises representing various transformations which Apache Beam Java SDK provides thanks to course https://www.udemy.com/course/apache-beam-a-hands-on-course-to-build-big-data-pipelines.

- AccountDepartmentWiseSummaryPipeline (Basic pipeline to read src\main\resources\dept_data.txt and retrieve attendance count of each employee of Account Department using MapElements/ Filter/ Group By transformations)
- AccountDepartmentWithAddOutput (Pipeline to extract distinct employee names and employee names having <=4 length using Pardo Additional Output i.e withOutPuts functionality )
- AccountDeptWiseSummarySideInputPipeline (Pipeline to extract attendance count of Account department employees while excluding list of specified employees (provided as side input in Pardo transformation) along with composite transformation )
- AccountHRDepartmentWiseMergedPipeline (Pipeline to retrieve Account and HR employees attendance count while demonstrating merge functionality of PCollections using Flatten transformation)
- AccountHRDepartmentWiseSummaryPipeline (Pipeline demonstrating branching of two transformations, One for HR department employees and another for Account department)
- CombineFnExercise (Pipeline demonstrating CombineFn transformation to calculate average of specified numbers)
- CompositeTransform (Pipeline retrieving HR and Account department employees attendance count demonstrating Composite Transformation)
- CreditCardSkipper (Pipeline extracting credit card users who skipped/ missed to do the payment on time. Reading src\main\resources\cards.txt. Currently in progress)
